﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;

namespace FF_PROG3_2014_UA93WK
{
    public class GameOverWindowVM:Bindable
    {
        string myresult;
        public string MyResults
        {
            get { return myresult; }
            set { myresult = value;
            OnPropertyChanged("MyResults");
            }
        }
        public void LoadSavedRecords()
        {
            try
            {
                using (StreamReader sr = new StreamReader("SCORES/highscores.txt"))
                {
                    while (!sr.EndOfStream)
                    {
                        string[] line = sr.ReadLine().Split(',');
                        SavedResult result = new SavedResult(line[0], int.Parse(line[1]), int.Parse(line[2]), line[3]);
                        PlayerList.Add(result);
                    }
                }
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Score File Not Found,Save Error, Your Score Not Saved!");
            }
        }
        ObservableCollection<SavedResult> playerlist;
        public ObservableCollection<SavedResult> PlayerList
        {
            get { return playerlist; }
            set
            {
                playerlist = value;
                OnPropertyChanged("Playerlist");
            }
        }
        public string CurrentName
        {
            get {return currentresult.Name;}
            set
            {
                currentresult.Name = value;
                OnPropertyChanged("CurrentName");
            }
        }

        SavedResult currentresult;
        public SavedResult Currentresult
        {
            get { return currentresult; }
            set { currentresult = value; }
        }
        public GameOverWindowVM(SavedResult x)
        {
            currentresult = x;
            MyResults = x.ToString();
            playerlist = new ObservableCollection<SavedResult>();
            LoadSavedRecords();
        }

    }
}
