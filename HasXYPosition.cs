﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FF_PROG3_2014_UA93WK
{
    interface HasXYPosition
    {
        double PositionX{get; set;}
        double PositionY { get; set;}
    }
}
