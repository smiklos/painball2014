﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FF_PROG3_2014_UA93WK
{
    /// <summary>
    /// Interaction logic for WindowShowScores.xaml
    /// </summary>
    public partial class WindowShowScores : Window
    {
        public WindowShowScores(SavedResult x)
        {
            InitializeComponent();
            GameOverWindowVM showscores = new GameOverWindowVM(x);
            this.DataContext = showscores;
        }
    }
}
