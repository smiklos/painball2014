﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FF_PROG3_2014_UA93WK
{
    public class SavedResult:Bindable
    {
        string name;
        public string Name
        {
            get { return name; }
            set { name = value;
            OnPropertyChanged("Name");}
        }
        int score;
        int seconds;
        string now;
        public SavedResult(string name,int score,int seconds,string now)
        {
            this.name = name; this.score = score; this.seconds = seconds;
            this.now = now;
        }
        public string SaveData()
        {
            string sd = "";
            sd = String.Format("{0},{1},{2},{3}", name, score, seconds, now);
            return sd;
        }
        public override string ToString()
        {
            return String.Format("{0},Score:{1},Played {2}sec,Died at {3}", name, score, seconds, now);
        }
    }
}
