﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;


namespace FF_PROG3_2014_UA93WK
{
    class PlayerGameObject:GameObject
    {
        public event EventHandler IChangedPosition;
        public override double PositionX 
        {
            get { return positionx; }
            set
            {
                if (IChangedPosition != null)
                { IChangedPosition(this, null); }
                positionx = value;
                OnPropertyChanged("PositionX");               
            }
        }
        public override double PositionY
        {
            get { return positiony; }
            set
            {
                if (IChangedPosition != null)
                { IChangedPosition(this, null); }
                positiony = value;
                OnPropertyChanged("PositionY");
            }
        }
        double angle;
      //  List<Bullet> MyShoots;
        Vector shooting_direction;
        public double getsetAngle
        {
            get { return angle; }
            set { angle = value;
            OnPropertyChanged("getsetAngle");
            }
        }

        public PlayerGameObject(double canvaswidth,double canvasheight,double newposx,double newposy)
            :base(canvaswidth,canvasheight,newposx,newposy,64)
        {
            getsetAngle = 0;
          //  MyShoots=new List<Bullet>();
        }
        internal void ChangeMyPlayerAngle(System.Windows.Point MyMousePointer)
        {
            Vector vectorme = new Vector(PositionX+32, PositionY+100);//a karakter szélességét és magasságát levonjuk
            Vector vectormouse = new Vector(MyMousePointer.X, MyMousePointer.Y);
            Vector FromMeToMouse = vectormouse-vectorme;
            shooting_direction = FromMeToMouse;
            getsetAngle =  -Vector.AngleBetween(FromMeToMouse, GameObject.vertical);
        }
        public Bullet Shoot()
        {
            Bullet newbullet = new Bullet(PositionX, PositionY, this, shooting_direction,angle);
           // MyShoots.Add(newbullet);
            return newbullet;
        }
    }
}
