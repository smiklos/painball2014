Painball
=======================

Introduction
------------

HU:
------------
Billentyű kiosztás:
W-Fel
S-Le
A-Balra
D-Jobbra
Bal Egér Gomb-Lövés
H-Előző játékosok Pontszámai


A Játék célja:
a zöld emberkének minél több pontot össze kell gyűjtenie.

Pontokat kap az idő függvényében, és hogy hány ellenfelet azaz piros emberkét
iktat ki festékszóró fegyverével. A játék akkor ér véget ha emberünket találat éri.
A pontatlan lövésekért pontvesztés jár.

Lövésünk irányát az egérmutató elhelyezkedése szabja meg, a mezőkön fedezékek is
elhelyezkednek, ha ezek mögé mozgatjuk játékosunkat akkor nem találhatja el lövés,
ez szintén érvényes az ellenséges játékosokra is,
azaz mi sem lőhetjük le őket ha fedezék mögött tartózkodnak.

A játékot nehezíti, hogy minden tizedik ellenfél kiiktatása után „x” százalék
esélyünk van, hogy egyszerre „y” számú új ellenség induljon támadásba
(egyébként csak egy indul öt másod-percenként).
Például: Kezdetben még csak „x”=20% esélyünk van az ellenség utánpótlására és
minden 10. találatnál nő 5%-al.

EN:
-----------
Keyboard:
W-up
S-down
A-left
D-right
Left mouse button - shoot
H-Previous scores


The goal of the game:
player has to get as much points as possible


Player gets point for time he/she survive and for shooting red players.
Player can also lose points when missing the target. 

You can controll the direction of the gun with mouse pointing on the target.



