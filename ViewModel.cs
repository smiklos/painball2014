﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FF_PROG3_2014_UA93WK
{
    
    
    class ViewModel:Bindable
    {
        
        int score;
        public int Score
        {
            get { return score; }
            set { score = value;
            OnPropertyChanged("Score");
            }
        }

        
        int numberofhitthetarget;
        public int Numberofhitthetarget
        {
            get { return numberofhitthetarget; }
            set { numberofhitthetarget = value;
            OnPropertyChanged("Numberofhitthetarget");
            OnPropertyChanged("MissedShots");
            }
        }
        public void OnHitEnemy()
        {
            Numberofhitthetarget += 1;
        }
        public int MissedShots { get { return numberofshoot - numberofhitthetarget; } }

        int numberofshoot;
        public int Numberofshot
        {
            get { return numberofshoot; }
            set { numberofshoot = value;
            OnPropertyChanged("Numberofshot");
            OnPropertyChanged("MissedShots");
            }
        }
        int BonusPointInEvery30sec;
        int seconds;
        public int Seconds
        {
            get { return seconds; }
        }
        public void AddSec()
        {
            seconds++;
            BonusPointInEvery30sec++;
            if (BonusPointInEvery30sec == 30)
            {
                Score += 10;
                BonusPointInEvery30sec = 0;
            }
            OnPropertyChanged("Time");           
        }
       
        public string Time
        {
            get { int hour = seconds / 3600;
                  int min = (seconds - (hour * 3600))/60;
                  int sec = seconds - hour * 3600 - min * 60;
                  string time = String.Format("{0}:{1}:{2}", hour, min, sec);
                  return time;
            }

        }
        bool pause;
        public bool Pause
        {
            get { return pause; }
            set { pause = value;
            OnPropertyChanged("Pause");
            }
        }

        List<Bindable> GameObjects;
        PlayerGameObject myPlayer;
        public PlayerGameObject getsetMyPlayer
        {
            get { return myPlayer; }
            set { myPlayer = value; }
        }
        public List<Bindable> getsetGameObjects
        {
            get { return GameObjects; }
            set { GameObjects = value; }
        }        
        public ViewModel(double cw,double ch)
        {
            pause = false;
            GameObjects = new List<Bindable>();
            myPlayer = new PlayerGameObject(cw, ch, 0, ch/2);
            score = 0;
            numberofhitthetarget = 0;
            numberofshoot = 0;
            BonusPointInEvery30sec = 0;
            R = new Random();
        }
        static Random R;
        internal bool IshallAddnewEnemy()
        {
            bool sendnewenemy=false;
            int chancetosendenemy = 20 + 5*(numberofhitthetarget / 10);
            if (R.Next(0,101)<chancetosendenemy || (seconds!=0 && (seconds % 5) == 0))
            {
                sendnewenemy = true;
            }
            return sendnewenemy;
        }
      /*  public void SaveMySore()
        {
            using (StreamWriter sr = new StreamWriter("highscores.txt", true))
            {
                sr.Write(String.Format("This is my HighScore!"));
            }
        }*/
    }
}
