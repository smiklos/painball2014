﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace FF_PROG3_2014_UA93WK
{
    public enum bulletsource
    {
        enemy, friendly
    }
    
    class Bullet:Bindable,HasXYPosition
    {
        public event EventHandler GameOver;
        public event EventHandler IKilledOne;
        public event EventHandler IMissedMyTarget;
        static int imagesize = 23;
        Image bulletimage;
        Rect area;
      /*  public Rect Area
        {
            get { return area; }
        }*/
        int speed=5;
        
        bulletsource thisbullet;
        public  bulletsource ThisBulletSources
        {
            get { return thisbullet; }
        }

        double bulletposx;
        public double PositionX
        {
            get { return bulletposx; }
            set { bulletposx = value;
            OnPropertyChanged("PositionX");
            }
        }

        double bulletposy;
        public double PositionY
        {
            get { return bulletposy; }
            set { bulletposy = value;
            OnPropertyChanged("PositionY");
            }
        }
        Vector shootingvector;
        public void ChangeX()
        {
            PositionX += shootingvector.X;
            area.X += shootingvector.X;
        }
        public void ChangeY()
        {
            PositionY += shootingvector.Y;
            area.Y += shootingvector.Y;
        }
        public void SetImage(Image myimage)
        {
            bulletimage = myimage;
        }
        public void RemoveImage()
        {
            bulletimage.Source = null;
        }
        
        public Bullet(double sourcex,double sourcey,GameObject shootingobject,Vector shooting_direction,double angle)
        {
            if (shootingobject is PlayerGameObject)
            {
                thisbullet = bulletsource.friendly;
            }
            else
            {
                thisbullet = bulletsource.enemy;
            }
            Vector unitvector = shooting_direction / shooting_direction.Length;
            this.shootingvector = unitvector * speed;
                                                         //0.5          /0.8
            Vector adjustbulletstartpoint=new Vector((sourcex+32),(sourcey+51));
            //Ha a szog 0 akkor a fenti sor jo különben modositjuk az eltolás vektorát a lenti függv.
            Vector ModAdjustStartVectorToAngle=ModBulletStartPoint(new Vector(-4.5,0),angle);
            PositionX = adjustbulletstartpoint.X+ModAdjustStartVectorToAngle.X+unitvector.X*32;
            PositionY = adjustbulletstartpoint.Y+ModAdjustStartVectorToAngle.Y+unitvector.Y*32;
            
            
        }
        public bool Move(double cw,double ch,List<Bindable> gameobjects,PlayerGameObject myplayer)
        {
            bool canimove=false;
            double distancebetweenmeandotherobject=0;
            if (PositionX < cw - imagesize / 2 && PositionX > 0
                && PositionY < ch - imagesize / 2 && PositionY > 0)
            {
                canimove = true;
            }
            else
            {
                if (IMissedMyTarget != null)
                { IMissedMyTarget(this, null); }
                return false;
            }
            if (this.ThisBulletSources == bulletsource.enemy
                && DistanceBetweenMeAndParameterObject(myplayer) < 25)
            {
                if (GameOver != null)
                {
                    GameOver(this, new EventArgs());
                }
               return false;
            }
            foreach (Bindable akt in gameobjects.ToList())
            {
                if (akt is HasXYPosition)
                {
                    distancebetweenmeandotherobject = 0;
                    distancebetweenmeandotherobject = DistanceBetweenMeAndParameterObject(akt as HasXYPosition);
                    if (distancebetweenmeandotherobject < 32 && !(akt is Bullet))
                    {
                        if ((akt is PlayerGameObject && thisbullet == bulletsource.friendly) || akt is Bullet )
                        {
                            canimove = true;
                            return canimove;
                        }
                        else if(akt is EnemyGameObject && thisbullet == bulletsource.friendly)
                        {
                            if (IKilledOne != null)
                            { IKilledOne(this,null); }
                            (akt as EnemyGameObject).RemoveImage();
                            gameobjects.Remove(akt);
                            return false;
                        }
                        else if (akt is GameObject && !(akt is PlayerGameObject) && !(akt is EnemyGameObject))
                        {
                            if (IMissedMyTarget != null)
                            { IMissedMyTarget(this, null); }
                            canimove = false;
                            return canimove;
                        }
                    }
                }
            }
            if (canimove)
            {
                ChangeX();
                ChangeY();
            }
            return canimove;
        }
        public Vector ModBulletStartPoint(Vector needtomod,double angle)
        {
            
            //A fegyver jobb vállon van ezért ey kicsit jobbra toljuk alaphejzetben,
            //de ez változik a forgásszöggel
            double radius = needtomod.Length;
            double newx= needtomod.X+radius*(Math.Cos((Math.PI/180)*angle));
            double newy = needtomod.Y+radius * (Math.Sin((Math.PI/180) *angle));
            Vector newvector = new Vector(newx,newy);
            return newvector; 
        }
        public double DistanceBetweenMeAndParameterObject(HasXYPosition akt)
        {
            double distance = 0;
            Vector mypositionvector = new Vector(this.PositionX, this.PositionY);
            Vector aktpositionvector = new Vector(akt.PositionX+32,
                                                  akt.PositionY+32);
            distance = Math.Abs((mypositionvector - aktpositionvector).Length);
            return distance;
        }
    }
}
