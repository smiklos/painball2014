﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace FF_PROG3_2014_UA93WK
{
    class EnemyGameObject:GameObject
    {
        static int shootingdensity = 200;//2sec
        double angle;
        Vector shooting_direction;
        public double getsetAngle
        {
            get { return angle; }
            set
            {
                angle = value;
                OnPropertyChanged("getsetAngle");
            }
        }
        public void ChangePlayerAngle(Vector target)
        {
            Vector vectorme = new Vector(PositionX, PositionY);//a karakter szélességét és magasságát levonjuk
            Vector FromMeToMouse = Vector.Subtract(target, vectorme);
            shooting_direction = FromMeToMouse;
            getsetAngle = -Vector.AngleBetween(FromMeToMouse, GameObject.vertical);
        }
        public Bullet Shoot()
        {
            Bullet newbullet = new Bullet(PositionX, PositionY, this, shooting_direction, angle);
            return newbullet;
        }
        public EnemyGameObject(double canvaswidth,double canvasheight,double newposx,double newposy)
            :base(canvaswidth,canvasheight,newposx,newposy,64)
        {
            randomtick = 0;
        }
        enum Directions 
        {
            Top,TopRight,Right,RightDown,Down,DownLeft,Left,LeftTop
        }
        public void RandomMoveToTarget(double cw,double ch,List<Bindable>gameobjects,PlayerGameObject myplayer)
        {
            ChangePlayerAngle(new Vector(myplayer.PositionX, myplayer.PositionY));
            double distancenow = DistanceBetweenMeAndParameterObject(myplayer,0,0);
            int sizeofstep = 1;
            int movex = 0;
            int movey = 0;
            int numberofdirections = Enum.GetNames(typeof(Directions)).Length;
            int randomdirection = GameObjectRandom.Next(0, numberofdirections);
            string randommove = Enum.GetName(typeof(Directions), randomdirection);
            switch (randommove)
            {
                case "Top": movey = -sizeofstep; break;
                case "TopRight": movey = -sizeofstep; movex = sizeofstep; break;
                case "Right": movex = sizeofstep; break;
                case "RightDown": movey = sizeofstep; movex = sizeofstep; break;
                case "Down": movey = sizeofstep; break;
                case "DownLeft": movey = sizeofstep; movex = -sizeofstep; break;
                case "Left": movex = -sizeofstep; break;
                case "LeftTop": movey = -sizeofstep; movex = -sizeofstep; break;
            }
            double distanceafterrandommove = DistanceBetweenMeAndParameterObject(myplayer, movex, movey);
            if (distancenow >= distanceafterrandommove)
            {
                this.Move(movex, movey, cw, ch, gameobjects, myplayer);
            }
        }

        int randomtick;
        public int Randomtick
        {
            get { return randomtick; }
            set {
                if (randomtick >= shootingdensity)
                { randomtick = 0; }
                else randomtick = value;
            }
        }
        internal bool IshallShoot()
        {
            return randomtick >= shootingdensity;
        }
    }
}
