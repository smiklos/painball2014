﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows;

namespace FF_PROG3_2014_UA93WK
{
    class InfoWindowVM:Bindable
    {
        string info;
        public string Info
        {
            get { return info; }
            set { info = value;
            OnPropertyChanged("Info");
            }
        }

        string score;
        public string Score
        {
            get { return score; }
            set { score = value;
            OnPropertyChanged("Info");
            }
        }
        public InfoWindowVM()
        {
            score = "";
            info = "";
            try
            {
                using (StreamReader sr = new StreamReader("INFO/info.txt"))
                {
                    while (!sr.EndOfStream)
                    {
                        info += sr.ReadLine();
                        info += "\n";
                    }
                }
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Info File Not Found, Info ERROR!");
            }
        }
    }
}
