﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.IO;
using System.Collections.ObjectModel;


namespace FF_PROG3_2014_UA93WK
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Random R;
        Canvas MainCanvas;
        ViewModel VM;
        public MainWindow()
        {
            InitializeComponent();
            MainCanvas = Content as Canvas;
            
        }
        private void Window_Loaded_1(object sender, System.Windows.RoutedEventArgs e)
        {
            R = new Random();
            VM = new ViewModel(MainCanvas.ActualWidth, MainCanvas.ActualHeight);
            MainCanvas.DataContext = VM;
            AddImageToMainCanvas("MyPlayer.tif",VM.getsetMyPlayer);
            RandomCoverObjects(MainCanvas,9);
            VM.getsetMyPlayer.IChangedPosition += setEnemyAngles;
            StartClockTicking();
            StartBulletTicking();
            StartMoveEnemiesClick();
        }
        public void SaveMySore(object sender,EventArgs e)
        {           
            PauseGame();
            SavedResult savedata = new SavedResult("", VM.Score, VM.Seconds,DateTime.Now.ToString());
            GameOverWindowVM GOVM = new GameOverWindowVM(savedata);
            GameOverWindow gameover = new GameOverWindow(GOVM);
            gameover.ShowDialog();
            if (gameover.DialogResult == true && GOVM.CurrentName!="")
            {
                using (StreamWriter sr = new StreamWriter("SCORES/highscores.txt", true))
                {
                    sr.WriteLine(GOVM.Currentresult.SaveData());
                }
            }
            this.Close();
        }
        
        enum CoverType 
        {
            rock,tree,sandbag 
        }
        private void RandomCoverObjects(Canvas thiscanvas,int numberofrandomobjects)
        {
            int numberofcovers = Enum.GetNames(typeof(CoverType)).Length;
            for (int i = 0; i < numberofrandomobjects; i++)
            {
                int randomcover = R.Next(0, numberofcovers);
                double randomx = R.Next(64, (int)thiscanvas.ActualWidth - 128);
                double randomy = R.Next(64, (int)thiscanvas.ActualHeight - 64);
                GameObject newcover = new GameObject(thiscanvas.ActualWidth,
                                                 thiscanvas.ActualHeight,
                                                 randomx,
                                                 randomy
                                                 ,64);

                AddImageToMainCanvas(String.Format("{0}.tif", Enum.GetName(typeof(CoverType), randomcover)), newcover);
                VM.getsetGameObjects.Add(newcover);
            }
        }
        DispatcherTimer TB;
        private void StartBulletTicking()
        {
            TB = new DispatcherTimer();
            TB.Interval = new TimeSpan(0, 0, 0, 0, 20); //50ms=20fps
            TB.Tick += Bullet_Tick;
            TB.Start();
        }
        private void Bullet_Tick(object sender, EventArgs e)
        {
            foreach (object akt in VM.getsetGameObjects.ToList())
            {
                if (akt is Bullet)
                {
                    Bullet bullet = akt as Bullet;
                    if (!bullet.Move(MainCanvas.ActualWidth, MainCanvas.ActualHeight, VM.getsetGameObjects, VM.getsetMyPlayer))
                    {
                        bullet.RemoveImage();
                        VM.getsetGameObjects.Remove(bullet);
                    }
                }
            }
           
        }

        DispatcherTimer T;
        private void StartClockTicking()
        {
            T = new DispatcherTimer();
            T.Interval = new TimeSpan(0, 0, 0, 1, 0); //50ms=20fps
            T.Tick += T_Tick;
            T.Start();
        }
        private void T_Tick(object sender, EventArgs e)
        {
            VM.AddSec();
            if (VM.IshallAddnewEnemy())
            {
                AddEnemyPlayer();
            }         
        }
        DispatcherTimer EnemyClick;
        private void StartMoveEnemiesClick() 
        {
            EnemyClick = new DispatcherTimer();
            EnemyClick.Interval = new TimeSpan(0, 0, 0, 0, 1);
            EnemyClick.Tick += MoveEnemies;
            EnemyClick.Start();
        }

        public void AddImageToMainCanvas(String fajl, Bindable datacontext)
        //a fájlnak léteznie kell a Solution Explorer IMAGES könyvtárában
            //és BuildAction = Content
            //Copy To Build Directory = Always
        {
            try
            {
                //setting up image
                Image newimage = new Image();

                if (datacontext is PlayerGameObject || datacontext is EnemyGameObject)
                {
                    newimage.Width = 64;
                    newimage.Height = 64;
                    newimage.RenderTransformOrigin = new Point(0.5, 0.8);
                    RotateTransform myRotateTransform = new RotateTransform();
                    //Binding1
                    BindingOperations.SetBinding(myRotateTransform, RotateTransform.AngleProperty, new Binding("getsetAngle"));
                    TransformGroup myTransformGroup = new TransformGroup();
                    myTransformGroup.Children.Add(myRotateTransform);
                    newimage.RenderTransform = myTransformGroup;
                    (datacontext as GameObject).SetImage(newimage);
                }
                else if (datacontext is Bullet)
                {
                    Bullet newbullet = datacontext as Bullet;
                    newimage.Width = 16;
                    newimage.Height = 16;
                    newbullet.SetImage(newimage);
                    if (newbullet.ThisBulletSources == bulletsource.enemy)
                    {
                        newbullet.GameOver += SaveMySore;
                    }
                }
                else if (datacontext is GameObject &&
                    !(datacontext is EnemyGameObject) &&
                    !(datacontext is PlayerGameObject))
                {
                    newimage.Width = 64;
                    newimage.Height = 64;
                    newimage.RenderTransformOrigin = new Point(0.5, 0.5);
                    RotateTransform myRotateTransform = new RotateTransform();
                    //Binding1
                    BindingOperations.SetBinding(myRotateTransform, RotateTransform.AngleProperty, new Binding("getsetAngle"));
                    TransformGroup myTransformGroup = new TransformGroup();
                    myTransformGroup.Children.Add(myRotateTransform);
                    newimage.RenderTransform = myTransformGroup;
                    (datacontext as GameObject).SetImage(newimage);
                }
                //making bitmap for image
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.UriSource = new Uri(String.Format("/FF_PROG3_2014_UA93WK;component/IMAGES/{0}", fajl), UriKind.RelativeOrAbsolute);
                bi.EndInit();
                newimage.Source = bi;
                //binding 2
                newimage.DataContext = datacontext;
                newimage.SetBinding(Canvas.LeftProperty, new Binding("PositionX"));
                newimage.SetBinding(Canvas.TopProperty, new Binding("PositionY"));
                MainCanvas.Children.Add(newimage);
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("File not Found Error At AddImageToMainCanvas()");
            }
        }

        private void Window_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (!VM.Pause)
            {
                switch (e.Key)
                {
                    case Key.W: VM.getsetMyPlayer.Move(0, -10, MainCanvas.Width, MainCanvas.Height, VM.getsetGameObjects, VM.getsetMyPlayer); break;
                    case Key.S: VM.getsetMyPlayer.Move(0, +10, MainCanvas.Width, MainCanvas.Height, VM.getsetGameObjects, VM.getsetMyPlayer); break;
                    case Key.A: VM.getsetMyPlayer.Move(-10, 0, MainCanvas.Width, MainCanvas.Height, VM.getsetGameObjects, VM.getsetMyPlayer); break;
                    case Key.D: VM.getsetMyPlayer.Move(+10, 0, MainCanvas.Width, MainCanvas.Height, VM.getsetGameObjects, VM.getsetMyPlayer); break;
                    case Key.H: ShowHighScores(); break;
                   // case Key.N: AddEnemyPlayer(); break; //teszteléshez használtam
                   // case Key.F: AllEnemyShoot(); break; //teszteléshez használtam
                }
            }
        }

        private void ShowHighScores()
        {
            PauseGame();
            WindowShowScores scores = new WindowShowScores(new SavedResult("",1,1,""));
            scores.ShowDialog();
            StartGame();
        }

        private void AllEnemyShoot()
        {
            foreach(object akt in VM.getsetGameObjects.ToList())
            {
                if(akt is EnemyGameObject)
                {
                    EnemyGameObject aktenemy = (akt as EnemyGameObject);
                    Bullet newbullet = aktenemy.Shoot();
                    VM.getsetGameObjects.Add(newbullet);
                    AddImageToMainCanvas("RedBullet.tif", newbullet);       
                }
            }
        }

        private void Canvas_MouseMove_1(object sender, MouseEventArgs e)//canvas mouse move
        {
            if (!VM.Pause)
            {
                Point MyMousePointer = e.GetPosition(this);
                VM.getsetMyPlayer.ChangeMyPlayerAngle(MyMousePointer);
            }
        }

        private void Window_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            Bullet newbullet=VM.getsetMyPlayer.Shoot();
            VM.getsetGameObjects.Add(newbullet);
            AddImageToMainCanvas("YellowBullet.tif", newbullet);
            VM.Numberofshot += 1;
            newbullet.IKilledOne += IncraseNumberOfHits;
            newbullet.IKilledOne += AddScoreForKill;
            newbullet.IMissedMyTarget += RemoveScoreForMissedTarget;
        }

        public void IncraseNumberOfHits(object sender,EventArgs e)
        {
            VM.Numberofhitthetarget += 1;
        }
        public void setEnemyAngles(object sender,EventArgs e)
        {
            Vector myplayer = new Vector(VM.getsetMyPlayer.PositionX, VM.getsetMyPlayer.PositionY);
            foreach (object akt in VM.getsetGameObjects.ToList())
            {
                if (akt is EnemyGameObject)
                {
                    (akt as EnemyGameObject).ChangePlayerAngle(myplayer);
                }
            }
        }

        public void AddEnemyPlayer()
        {
            Double randomx;
            Double randomy;

            randomx = MainCanvas.Width - 64;
            randomy=R.Next(0, (int)MainCanvas.Height-64);
            EnemyGameObject newenemy = new EnemyGameObject(MainCanvas.ActualWidth, MainCanvas.ActualHeight, randomx, randomy);
            AddImageToMainCanvas("RedSoldier.tif", newenemy);
            VM.getsetGameObjects.Add(newenemy);
        }
        public void AddScoreForKill(object sender,EventArgs e)
        {
            VM.Score += 5;
        }
        public void RemoveScoreForMissedTarget(object sender,EventArgs e)
        {
            VM.Score -= 1;
        }

        
        public void MoveEnemies(object sender, EventArgs e)
        {
            
            foreach (object akt in VM.getsetGameObjects.ToList())
            {
                if (akt is EnemyGameObject)
                {
                    EnemyGameObject aktenemy = (akt as EnemyGameObject);
                    aktenemy.Randomtick++;
                    aktenemy.RandomMoveToTarget(MainCanvas.ActualWidth,
                                                                 MainCanvas.ActualHeight,
                                                                 VM.getsetGameObjects,
                                                                 VM.getsetMyPlayer);
                    if (aktenemy.IshallShoot())
                    {                      
                        Bullet newbullet = aktenemy.Shoot();
                        VM.getsetGameObjects.Add(newbullet);
                        AddImageToMainCanvas("RedBullet.tif", newbullet);
                    }

                }
            }
        }

        private void Canvas_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            Bullet newbullet = VM.getsetMyPlayer.Shoot();
            VM.getsetGameObjects.Add(newbullet);
            AddImageToMainCanvas("YellowBullet.tif", newbullet);
            VM.Numberofshot += 1;
            newbullet.IKilledOne += IncraseNumberOfHits;
            newbullet.IKilledOne += AddScoreForKill;
            newbullet.IMissedMyTarget += RemoveScoreForMissedTarget;
        }


        private void Button_Click_1(object sender, RoutedEventArgs e)//pause button
        {
            if (!VM.Pause)
            {
                PauseGame();
                VM.Pause = true;
            }
            else { StartGame(); VM.Pause = false; }
        }
        public void PauseGame() 
        {
            T.Stop();
            EnemyClick.Stop();
            TB.Stop();
        }
        public void StartGame()
        {
            T.Start();
            EnemyClick.Start();
            TB.Start();
            VM.Pause = false;
        }

        private void ButtonClickInfo(object sender, RoutedEventArgs e)
        {
           
            PauseGame();
            InfoWindow info = new InfoWindow();
            info.ShowDialog();
            StartGame();
        }
    }
    

    
}
