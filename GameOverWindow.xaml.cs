﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FF_PROG3_2014_UA93WK
{
    /// <summary>
    /// Interaction logic for GameOverWindow.xaml
    /// </summary>
    public partial class GameOverWindow : Window
    {
       
        public GameOverWindow(GameOverWindowVM VM)
        {
            InitializeComponent();
            DataContext = VM;
        }

        private void ButtonClickOK(object sender, RoutedEventArgs e)
        {

            this.DialogResult = true;
        }

        private void YourNamePreviewKeyDown(object sender, KeyEventArgs e)
        {
            if ((sender as TextBox).Text.Length >= 10 && e.Key!=Key.Back)
            {
                e.Handled = true;
            }
        }

    }
}
