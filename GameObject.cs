﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.IO;

namespace FF_PROG3_2014_UA93WK
{
    class GameObject:Bindable,HasXYPosition
    {
      //  public event EventHandler GameOver;
        static Random R;
        public static Random GameObjectRandom
        {
            get { return GameObject.R; }
        }
        protected static Vector horizontal = new Vector(100, 0);
        protected static Vector vertical = new Vector(0, -100);
       
        protected Double positionx;
        public virtual Double PositionX
        {
            get { return positionx; }
            set { positionx = value;
            OnPropertyChanged("PositionX");
            }
        }

        protected Double positiony;
        public virtual Double PositionY
        {
            get { return positiony; }
            set { positiony = value;
            OnPropertyChanged("PositionY");
            }
        }
        Image objectimage;
        public void SetImage(Image myimage)
        {
            objectimage = myimage;
        }
        public void RemoveImage()
        {
            objectimage.Source = null;
        }
        public GameObject(double canvaswidth,double canvasheight,double newposx,double newposy,int heightinpixel)
        {
            PositionX = newposx;
            PositionY = newposy;
            R = new Random();
        }
        public bool Move(double diffx,double diffy,double cw, double ch, List<Bindable> gameobjects, PlayerGameObject myplayer)
        {
            bool canimove = true;
            double distancebetweenmeandotherobject = 0;
            //canvas széleit vizsgáljuk
            if (!(PositionX + diffx > cw - 32 || PositionX + diffx < -32
                || PositionY + diffy > ch - 64 || PositionY + diffy < -32))
            {
                //más objektumokkal való ütközés
                foreach (Bindable akt in gameobjects.ToList())
                {
                    if (akt is HasXYPosition)
                    {
                        distancebetweenmeandotherobject = 0;
                        distancebetweenmeandotherobject = DistanceBetweenMeAndParameterObject(akt as HasXYPosition, diffx, diffy);
                        if (distancebetweenmeandotherobject < 50)
                        {
                            /*if (akt is Bullet && this is PlayerGameObject && (akt as Bullet).ThisBulletSources == bulletsource.enemy)
                            {
                                canimove = false;
                                if (GameOver != null)
                                {
                                    GameOver(this, new EventArgs());
                                }
                            }*/
                            if (akt is Bullet && this is EnemyGameObject && (akt as Bullet).ThisBulletSources == bulletsource.friendly)
                            {
                                canimove = false;
                            }
                            else if (akt is GameObject && !(akt is EnemyGameObject) && !(akt is PlayerGameObject))
                            {
                                canimove = false;
                            }
                        }

                    }
                }
            }
            else { canimove = false; }
            if (canimove)
            {
                PositionX += diffx;
                PositionY += diffy;
            }
            return canimove;
        }
        public double DistanceBetweenMeAndParameterObject(HasXYPosition akt,double diffx,double diffy)
        {
            Vector aktpositionvector;
            double distance = 0;
            Vector mypositionvector = new Vector(this.PositionX+32+diffx, this.PositionY+51+diffy);
            if (akt is Bullet)
            {
                aktpositionvector = new Vector(akt.PositionX,
                                                      akt.PositionY);
            }
            else
            {
                aktpositionvector = new Vector(akt.PositionX+32,
                                                      akt.PositionY+32); 
            }
            distance = Math.Abs((mypositionvector - aktpositionvector).Length);
            return distance;
        }
        

    }
}
